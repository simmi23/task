import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  imagePath = './assets/customer.jpg';
  name1 = 'customer1';
  name2 = 'customer2';
  anotherComp = false;
  thirdComp = false;
  constructor() { }

  ngOnInit() {
  }

  displayAnotherComp() {
    this.anotherComp = true;
  }

  callThirdComp(valThirdComp: boolean) {
    this.thirdComp = valThirdComp;
  }
}
