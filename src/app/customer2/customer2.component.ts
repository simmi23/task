import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-customer2',
  templateUrl: './customer2.component.html',
  styleUrls: ['./customer2.component.scss']
})
export class Customer2Component implements OnInit {
  imagePath = './assets/customer.jpg';
  @Output() displayThirdComp = new EventEmitter<boolean>();
  aComp = false;
  constructor() { }

  ngOnInit() {
  }

  displayAnotherComp() {
    this.aComp = true;
    this.displayThirdComp.emit(this.aComp);
  }
}
