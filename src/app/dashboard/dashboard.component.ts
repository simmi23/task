import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
   displayForm = false;
   // @ts-ignore
  @ViewChild('f') signUpForm: ngForm;
   name = '';
   email = '';
   userName = '';
  constructor() { }

  ngOnInit() {
  }

  onCreateClick() {
    this.displayForm = true;
  }

  onSubmit() {
     this.name = this.signUpForm.value.name;
     this.email = this.signUpForm.value.email;
     this.userName = this.signUpForm.value.username;
  }
}
