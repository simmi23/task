import {RouterModule, Routes} from '@angular/router';
import {CustomerComponent} from './customer/customer.component';
import {NgModule} from '@angular/core';
import {Customer2Component} from './customer2/customer2.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {OrdersComponent} from './orders/orders.component';
import {CustomersComponent} from './customers/customers.component';

const appRoutes: Routes = [
  {path: 'customers', component: CustomersComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'orders', component: OrdersComponent}
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
