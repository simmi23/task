import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  itemValue: string;
  imgCust = './assets/cmp.jpg';
  imgSales = './assets/s.png'

  onItemsClicked(sidebarValue: string) {
    this.itemValue = sidebarValue;
  }
}
